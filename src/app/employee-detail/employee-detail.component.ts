import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { EmployeeService } from '../employee.service';
import { Employee } from '../employee.model';
import { MatCardModule } from '@angular/material/card';
import { MatButton, MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css'],
  standalone: true,
  imports: [CommonModule, MatCardModule, MatButton, MatButtonModule],
})
export class EmployeeDetailComponent implements OnInit {
  employee!: Employee;
  previousSearchTerm: string | null = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private employeeService: EmployeeService
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.previousSearchTerm = this.route.snapshot.queryParams['search'];

    this.employee = this.employeeService.getEmployeeById(id) as Employee;
  }

  formatSalary(salary: number): string {
    return new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR',
    }).format(salary);
  }

  onOk(): void {
    this.router.navigate(['/employees'], {
      queryParams: { search: this.previousSearchTerm },
    });
  }
}
