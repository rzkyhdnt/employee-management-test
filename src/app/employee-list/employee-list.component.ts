import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { Router } from '@angular/router';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { MatCardModule } from '@angular/material/card';
import { MatFormField, MatLabel } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { Employee } from '../employee.model';
import { EmployeeService } from '../employee.service';
import { MatButtonModule } from '@angular/material/button';
import { MatInput } from '@angular/material/input';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  standalone: true,
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css'],
  imports: [
    MatCardModule,
    MatFormField,
    MatLabel,
    FormsModule,
    MatTableModule,
    MatPaginator,
    CommonModule,
    MatIconModule,
    MatSortModule,
    MatButtonModule,
    MatInput,
  ],
})
export class EmployeeListComponent implements OnInit {
  displayedColumns: string[] = [
    'username',
    'firstName',
    'lastName',
    'email',
    'birthDate',
    'basicSalary',
    'status',
    'group',
    'description',
    'actions',
  ];

  employees: Employee[] = [];

  dataSource = new MatTableDataSource<Employee>();

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  searchUsername: string = '';
  searchStatus: string = '';

  constructor(
    private router: Router,
    private matPaginatorIntl: MatPaginatorIntl,
    private employeeService: EmployeeService,
    private snackBar: MatSnackBar
  ) {
    const employees: Employee[] = this.employeeService.getEmployees();
    this.dataSource = new MatTableDataSource(employees);

    this.matPaginatorIntl = new MatPaginatorIntl();
    this.matPaginatorIntl.itemsPerPageLabel = 'Items per page:';
    this.matPaginatorIntl.nextPageLabel = 'Next page';
    this.matPaginatorIntl.previousPageLabel = 'Previous page';
    this.matPaginatorIntl.firstPageLabel = 'First page';
    this.matPaginatorIntl.lastPageLabel = 'Last page';
    this.matPaginatorIntl.getRangeLabel = function (
      page: number,
      pageSize: number,
      length: number
    ) {
      if (length === 0 || pageSize === 0) {
        return `0 of ${length}`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      const endIndex =
        startIndex < length
          ? Math.min(startIndex + pageSize, length)
          : startIndex + pageSize;
      return `${startIndex + 1} – ${endIndex} of ${length}`;
    };
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
    this.employees = this.employeeService.getEmployees();
  }

  sortData(column: keyof Employee, direction: string) {
    this.dataSource.sort = this.sort;
    this.dataSource.sort.active = column;
    this.dataSource.sort.direction = direction as any;
  }

  applyFilter() {
    this.dataSource.filterPredicate = (data: Employee, filter: string) => {
      const usernameMatch = data.username
        .toLowerCase()
        .includes(this.searchUsername.toLowerCase());
      const statusMatch = data.status
        .toLowerCase()
        .includes(this.searchStatus.toLowerCase());
      return usernameMatch && statusMatch;
    };
    this.dataSource.filter = 'filter';
  }

  loadEmployees(): void {
    this.employees = this.employeeService.getEmployees();
    this.dataSource.data = this.employees;
  }

  deleteEmployee(employeeId: number): void {
    const confirmation = confirm(
      'Are you sure you want to delete this employee?'
    );
    if (confirmation) {
      this.employeeService.deleteEmployee(employeeId);
      this.loadEmployees();
    }
    this.snackBar.open('Delete Success!', 'Close', {
      duration: 3000,
      panelClass: ['red'],
      verticalPosition: 'top',
    });
  }

  navigateToAddEmployee() {
    this.router.navigate(['/add-employee']);
  }

  navigateToEditEmployee(employeeId: number) {
    this.router.navigate([`/edit-employee`, employeeId]);
  }

  navigateToDetailEmployee(employeeId: number) {
    this.router.navigate([`/detail-employee`, employeeId]);
  }
}
