import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { EmployeeService } from '../employee.service';
import { Employee } from '../employee.model';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  standalone: true,
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css'],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
  ],
})
export class EmployeeEditComponent implements OnInit {
  employeeForm: FormGroup;
  detailEmployee = {} as Employee;

  groups: string[] = [
    'Group1',
    'Group2',
    'Group3',
    'Group4',
    'Group5',
    'Group6',
    'Group7',
    'Group8',
    'Group9',
    'Group10',
  ];

  maxDate: Date = new Date();

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    private employeeService: EmployeeService,
    private snackBar: MatSnackBar
  ) {
    this.employeeForm = this.fb.group({
      username: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      birthDate: ['', [Validators.required, this.birthDateValidator]],
      basicSalary: ['', [Validators.required, Validators.pattern(/^\d+$/)]],
      status: ['', Validators.required],
      group: ['', Validators.required],
      description: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.loadEmployee();
  }

  birthDateValidator(control: AbstractControl): { [key: string]: any } | null {
    const inputDate = new Date(control.value);
    const today = new Date();
    return inputDate > today ? { invalidDate: true } : null;
  }

  loadEmployee(): void {
    const id = this.route.snapshot.params['id'];
    const data = this.employeeService.getEmployeeById(id) as Employee;

    if (data) {
      this.employeeForm = this.fb.group({
        username: [data.username, Validators.required],
        firstName: [data.firstName, Validators.required],
        lastName: [data.lastName, Validators.required],
        email: [data.email, [Validators.required, Validators.email]],
        birthDate: [data.birthDate, Validators.required],
        basicSalary: [
          data.basicSalary,
          [Validators.required, Validators.pattern(/^\d+$/)],
        ],
        status: [data.status, Validators.required],
        group: [data.group, Validators.required],
        description: [data.description, Validators.required],
      });
      console.log(data, 'employee');
    } else {
      console.error('Employee not found');
    }
  }

  onSubmit() {
    const id = this.route.snapshot.params['id'];
    if (this.employeeForm.valid) {
      const newEmployee = this.employeeForm.value;
      this.employeeService.updateEmployee({ ...newEmployee, id: id });
      this.detailEmployee = this.employeeService.getEmployeeById(
        id
      ) as Employee;
      this.snackBar.open('Edit Employee Success!', 'Close', {
        duration: 3000,
        panelClass: ['blue'],
        verticalPosition: 'top',
      });
      this.router.navigate(['/employees']);
    }
  }

  onCancel() {
    this.router.navigate(['/employees']);
  }
}
