import { Injectable } from '@angular/core';
import { Employee } from './employee.model';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  private employees: Employee[] = this.generateDummyEmployees();

  constructor() {}

  generateDummyEmployees(): Employee[] {
    const employees: Employee[] = [];

    for (let i = 0; i < 100; i++) {
      const id = i;
      const username = `user_${i}`;
      const firstName = `First${i}`;
      const lastName = `Last${i}`;
      const email = `user${i}@example.com`;
      const birthDate = new Date(1980 + i, i % 12, (i % 28) + 1);
      const basicSalary = 50000 + i * 1000;
      const status = i % 2 === 0 ? 'Active' : 'Inactive';
      const group =
        i % 3 === 0 ? 'Group A' : i % 3 === 1 ? 'Group B' : 'Group C';
      const description = `Description${i}`;

      const employee: Employee = {
        id,
        username,
        firstName,
        lastName,
        email,
        birthDate,
        basicSalary,
        status,
        group,
        description,
      };

      employees.push(employee);
    }

    return employees;
  }

  getEmployees(): Employee[] {
    return this.employees;
  }

  addEmployee(employee: Employee) {
    this.employees.unshift(employee);
  }

  updateEmployee(employee: Employee): void {
    const index = this.employees.findIndex(
      (emp) => emp.username === employee.username
    );
    if (index !== -1) {
      this.employees.splice(index, 1, employee);
    }
  }

  getEmployeeById(id: number): Employee | undefined {
    return this.employees.find((emp) => emp.id == id);
  }

  deleteEmployee(id: number): void {
    this.employees = this.employees.filter((employee) => employee.id !== id);
  }
}
